/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mhaji <mhaji@student.42wolfsburg.de>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 02:09:06 by coder             #+#    #+#             */
/*   Updated: 2022/03/29 22:31:53 by mhaji            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strlen(char *str)
{
	int	count;

	count = 0;
	while (*str != '\0')
	{
		count++;
		str++;
	}
	return (count);
}

/*#include <stdio.h>
#include <string.h>
int	main(void)
{
	int a;
	int b;
	char str[]="Munir";
	char *c;
	c=str;
	a=ft_strlen(c);
	b=strlen(c);
	printf("%d",a);
	printf("%d",b);
}*/
