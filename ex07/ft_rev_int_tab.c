/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 02:26:24 by coder             #+#    #+#             */
/*   Updated: 2022/03/27 17:03:02 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_swap(int *a, int *b)
{
	int	c;

	c = *a;
	*a = *b;
	*b = c;
}

void	ft_rev_int_tab(int *tab, int size)
{
	int	count;

	count = 0;
	while (count < size)
	{
		ft_swap(&tab[count], &tab[size - 1]);
		count++;
		size--;
	}
}

/*#include <stdio.h>

int	main(void)
{
	int	c[5];
	int	i;
	int	*tab;
	int	size;

	size = 5;
	i = 0;
	c[0] = 10;
	c[1] = 20;
	c[2] = 30;
	c[3] = 40;
	c[4] = 50;
	printf("Before\n");
	while (i < size)
	{
		printf("%d, ", c[i]);
		i++;
	}
	i = 0;
	tab = c;
	ft_rev_int_tab(tab, size);
	printf("\nAfter\n");
	while (i < size)
	{
		printf("%d, ", c[i]);
		i++;
	}
}*/
