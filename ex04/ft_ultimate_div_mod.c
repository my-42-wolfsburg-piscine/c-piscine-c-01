/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_div_mod.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/22 23:21:30 by coder             #+#    #+#             */
/*   Updated: 2022/03/26 16:41:46 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_ultimate_div_mod(int *a, int *b)
{
	int	c;
	int	d;

	c = *a;
	d = *b;
	if (d != 0)
	{
		*a = c / d;
		*b = c % d;
	}
}

/*#include <stdio.h>
int	main(void)
{
	int a;
	int b;

	a=123;
	b=10;
	
	ft_ultimate_div_mod(&a, &b);
	
	printf("%d\n",a);
	printf("%d\n",b);
}*/
