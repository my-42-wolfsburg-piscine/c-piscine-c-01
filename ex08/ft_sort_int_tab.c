/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 14:04:02 by coder             #+#    #+#             */
/*   Updated: 2022/03/26 16:42:45 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_swap(int *a, int *b)
{
	int	c;

	c = *a;
	*a = *b;
	*b = c;
}

void	ft_sort_int_tab(int *tab, int size)
{
	int	i;
	int	count;

	i = 0;
	count = 0;
	while (count < size)
	{
		while (i < size - 1)
		{
			if (tab[i] > tab[i + 1])
			{
				ft_swap(&tab[i], &tab[i + 1]);
			}
			i++;
		}
		i = 0;
		count++;
	}
}

/*#include <stdio.h>
int main() {
    int test[] = {3,9,7,8,4,5};
    ft_sort_int_tab(test, 6);
    
    int i = 0;
    while (i < 6) {
        printf("%d, ", test[i]);
        i++;
    }
	printf("\b\b");
}*/
