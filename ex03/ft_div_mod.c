/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_div_mod.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: coder <coder@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/22 23:09:14 by coder             #+#    #+#             */
/*   Updated: 2022/03/26 16:41:38 by coder            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_div_mod(int a, int b, int *div, int *mod)
{
	if (b != 0)
	{
		*div = a / b;
		*mod = a % b;
	}
}

/*#include <stdio.h>
int	main(void)
{
	int d;
	int m;
	
	ft_div_mod(19, 3, &d, &m);
	
	printf("%d\n",d);
	printf("%d\n",m);
}*/
